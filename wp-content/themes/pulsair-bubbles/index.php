<?php
/**
 * The main template file.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

get_header();
?>
<section id="primary">
	<?php
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			get_template_part( 'content', get_post_format() );
		}
	}
	else {
    ?>
		<h2 class="entry-title"> <?php esc_html_e( 'No Posts Found.', 'pulsair-bubbles' ); ?> </h2>
	<?php } ?>
	<?php get_template_part( 'navigation', 'none' ); ?>
</section><!-- #primary -->
<?php
get_sidebar();
get_footer();
