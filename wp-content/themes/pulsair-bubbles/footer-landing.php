<?php
/**
 * The template for displaying the footer.
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

?>

    </main>
    <!-- end main -->
    <!-- Footer Start ============================================= -->
    <footer id="colophon" class="site-footer clearfix">
        <div class="site-info">
            <div class="container">
                <div class="lp-contact-info">
                <?php
                $contact_info = the_field('contact_info');

                if($contact_info) { ?>

                    <?php echo $contact_info; ?>

                <?php
                }
                ?>
                </div>
                <div class="copyright">&copy; <?php echo date('Y'); ?>
                    <a title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" target="_blank" href="https://www.pulsair.com"><?php echo get_bloginfo( 'name', 'display' ); ?></a>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </footer>
</div> <!-- end #page -->
<?php wp_footer(); ?>
<?php
$d_host = $_SERVER['HTTP_HOST'];
if ( $d_host == 'www.pulsair.com' or $d_host == 'pulsair.com' ) {
?>
    <!-- Global site tag (gtag.js) - Google Ads: 997263229 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-997263229"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-997263229');
</script>
<?php } ?>
</body>
</html>
