<?php
/**
 * The template for displaying all pages.
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

get_header();
?>
<section id="primary">
	<nav class="breadcrumb home">
		<?php the_breadcrumb(); ?>
	</nav> <!-- .breadcrumb -->
	<?php
	if ( has_post_thumbnail() ) {
    ?>
		<figure class="post-featured-image">
			<a href="<?php the_permalink(); ?>" title="<?php echo the_title_attribute('echo=0'); ?>">
				<?php the_post_thumbnail(); ?>
			</a>
		</figure><!-- end.post-featured-image  -->

	<?php
    }
	?>

	<?php
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
            ?>
			<div class="entry-content">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
				<?php
                the_content();
				wp_link_pages(
                     array(
						 'before'            => '<div style="clear: both;"></div><div class="pagination clearfix">' . esc_html_e( 'Pages:', 'pulsair-bubbles' ),
						 'after'             => '</div>',
						 'link_before'       => '<span>',
						 'link_after'        => '</span>',
						 'pagelink'          => '%',
						 'echo'              => 1,
					 )
                    );
                ?>
			</div> <!-- entry-content clearfix-->
			<?php comments_template(); ?>
		<?php
        }
	} else {
    ?>
		<h1 class="entry-title"> <?php esc_html_e( 'No Posts Found.', 'pulsair-bubbles' ); ?> </h1>
		<?php
	}
    ?>
</section><!-- #primary -->
<?php
get_sidebar();
get_footer();
