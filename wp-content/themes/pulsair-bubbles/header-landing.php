<?php
/**
* Displays the header content
*
* @package Theme Pulsair
* @subpackage Pulsair Bubbles
* @since Pulsair Bubbles 1.0
*/
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	<?php get_template_part( 'template-parts/favicons' ); ?>
</head>
<body <?php body_class(); ?>>
	<!-- Masthead ============================================= -->
	<header id="masthead" class="site-header top-header">
		<div class="">
			<div class="container clearfix psa--site-header">
				<div id="site-branding" class="psa--site-branding">
					<?php if ( get_theme_mod( 'company_logo' ) ) : ?>
						<a href="https://www.pulsair.com" id="site-logo" class="psa--site-logo site-header__logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							<img src="<?php echo get_theme_mod( 'company_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'description' ) ); ?>" data-rjs="2" data-no-lazy="1">
						</a>
					<?php endif; ?>
				</div>
				<div id="site-branding" class="psa--site-title">
					<?php if ( get_theme_mod( 'company_logo' ) ) : ?>
						<p id="site-title" class="psa--site-title"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'description' ); ?>" rel="home"> <?php bloginfo( 'description' ); ?></a></p>
					<?php else : ?>
						<hgroup>
							<h1 class="site-title"><a href="https://www.pulsair.com>" title="<?php echo esc_attr( get_bloginfo( 'name', 'description' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<p class="site-description"><?php bloginfo( 'description' ); ?></p>
						</hgroup>
					<?php endif; ?>
				</div>
				<div class="top-request aaa psa--site-contact">
					<ul>

						<?php
						$lp_phone = get_field('landing_page_phone_number');
						$lp_text  = get_field('landing_page_text');

						if ( $lp_text ) {
							?>
							<li><a href="/contact" title="<?php echo $lp_text; ?>"><?php echo $lp_text; ?></a></li>
						<?php } else { ?>
							<li><a href="/contact" title="Contact Us">Contact</a></li>
							<?php
						}

						if ( $lp_phone ) {
							?>
							<li><a href="tel:<?php echo $lp_phone; ?>"><?php echo $lp_phone; ?></a></li>
						<?php } else { ?>
							<li><a href="tel:800.582.7797">800.582.7797</a></li>
						<?php } ?>
						<li>
							<div id="search-toggle" class="header-search"></div>
							<div id="search-box" class="my-search-box clearfix">
								<?php get_search_form(); ?>
							</div>  <!-- end #search-box -->
						</li>
					</ul>
				</div>



			</div> <!-- end .container -->
		</div> <!-- end .top-header -->
		<!-- Main Header============================================= -->
	</header> <!-- end #masthead -->
	<!-- Main Page Start ============================================= -->
	<main id="main-content" class="page--content container clearfix">
