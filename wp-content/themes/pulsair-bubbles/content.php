<?php
/**
 * The template for displaying content.
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

$bubbles_content_format = get_post_format();
?>
<article <?php post_class('post-format' . ' format-' . $bubbles_content_format); ?> id="post-<?php the_ID(); ?>">
	<?php
	if ( has_post_thumbnail() ) {
    ?>
		<figure class="post-featured-image">
			<a href="<?php the_permalink(); ?>" title="<?php echo the_title_attribute('echo=0'); ?>">
				<?php the_post_thumbnail('blog-hero'); ?>
			</a>
		</figure><!-- end.post-featured-image  -->
	<?php } ?>
	<header class="entry-header">
		<h2 class="entry-title"> <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> <?php the_title(); ?> </a> </h2> <!-- end.entry-title -->

		<div class="entry-meta">
			<?php
			$bubbles_content_format = get_post_format();
			if ( current_theme_supports( 'post-formats', $bubbles_content_format ) ) {
				printf(
                     '<span class="entry-format">%1$s<a href="%2$s">%3$s</a></span>',
				sprintf( ''),
				esc_url( get_post_format_link( $bubbles_content_format ) ),
				get_post_format_string( $bubbles_content_format )
			);
		}
        ?>
		<span class="author vcard"><?php esc_html_e('Author :', 'pulsair-bubbles'); ?><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php the_author(); ?>">
			<?php the_author(); ?> </a></span>
			<span class="posted-on"><?php esc_html_e('Date  :', 'pulsair-bubbles'); ?><a title="<?php echo esc_attr( get_the_time() ); ?>" href="<?php the_permalink(); ?>"> <?php the_time( get_option( 'date_format' ) ); ?> </a></span>
			<?php if ( comments_open() ) { ?>
				<span class="comments"><?php esc_html_e('Comment  :', 'pulsair-bubbles'); ?>
					<?php comments_popup_link( esc_html_e( 'No Comments', 'pulsair-bubbles' ), esc_html_e( '1 Comment', 'pulsair-bubbles' ), esc_html_e( '% Comments', 'pulsair-bubbles' ), '', esc_html_e( 'Comments Off', 'pulsair-bubbles' ) ); ?> </span>
				<?php } ?>
			</div> <!-- end .entry-meta -->

		</header> <!-- end .entry-header -->
		<div class="entry-content">
			<?php the_excerpt(); ?>
		</div> <!-- end .entry-content -->
		<footer class="entry-footer">
			<span class="cat-links">
				<?php
				esc_html_e('Category: ', 'pulsair-bubbles');
				the_category(', ');
				?>
			</span> <!-- end .cat-links -->
			<?php
            $bubbles_tag_list = get_the_tag_list( '', ', ' );
			if ( ! empty($bubbles_tag_list) ) {
            ?>
				<span class="tag-links">
					<?php
					esc_html_e('tag:', 'pulsair-bubbles');
					echo $bubbles_tag_list;
					?>
				</span> <!-- end .tag-links -->
			<?php
            }
			?>
			<a class="more-link" title="<?php echo the_title_attribute('echo=0'); ?>" href="<?php the_permalink(); ?>"><?php esc_html_e('Read More', 'pulsair-bubbles'); ?></a>
			</footer> <!-- .entry-meta -->


		</article>
