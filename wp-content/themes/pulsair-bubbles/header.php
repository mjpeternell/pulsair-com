<?php
/**
 * Displays the header content
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

?>

<?php
$bubbles_theme_option = '';
if ( get_theme_mod( 'company_logo' ) ) {
	$bubbles_theme_option = get_theme_mod( 'theme_option_setting' );
}

?>
<!DOCTYPE html>
<html class="no-js <?php echo esc_attr($bubbles_theme_option); ?>" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
<?php get_template_part( 'template-parts/favicons' ); ?>
</head>
<body <?php body_class(); ?>>
<!-- Masthead ============================================= -->
<header id="masthead" class="site-header top-header ">
		<div class="container clearfix">
			<div id="site-branding" class="site-header__branding">
				<?php if ( get_theme_mod( 'company_logo' ) ) : ?>
					<?php
					$bubbles_company_logo = get_theme_mod( 'company_logo' );
					?>
					<a href="https://www.pulsair.com" id="site-logo" class="site-header__logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php echo esc_url($bubbles_company_logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'description' ) ); ?>" data-rjs="2" data-no-lazy="1">
					</a>
					<p id="site-title" class="site-header__title"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'description' ); ?>" rel="home"> <?php bloginfo( 'description' ); ?></a></p>
				<?php else : ?>
					<hgroup>
						<h1 class="site-title"><a href="https://www.pulsair.com>" title="<?php echo esc_attr( get_bloginfo( 'name', 'description' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<p class="site-description"><?php bloginfo( 'description' ); ?></p>
					</hgroup>
				<?php endif; ?>
			</div>
				<div class="top-request site-header__links">
					<ul>
						<li><a href="/contact" title="Contact Us">Contact</a></li>
						<li><a href="tel:800.582.7797">800.582.7797</a></li>
						<li>
							<div id="search-toggle" class="header-search"></div>
							<div id="search-box" class="my-search-box clearfix">
								<?php get_search_form(); ?>
							</div>  <!-- end #search-box -->
						</li>
					</ul>
				</div>
				<div class="menu-toggle">
					<div class="line-one"></div>
					<div class="line-two"></div>
					<div class="line-three"></div>
				</div>
		</div> <!-- end .container -->
	<!-- Main Header============================================= -->
	<div id="sticky_header" class="site-header__sticky sticky-header">


			<!-- Main Nav ============================================= -->
			<?php
				if ( has_nav_menu('primary') ) {
                ?>
			<?php
            $args = array(
				'theme_location' => 'primary',
				'container'      => '',
				'items_wrap'     => '<ul id="primary-menu" class="menu nav-menu site-header__nav-menu">%3$s</ul>',
			);
                ?>
			<nav id="site-navigation" class="main-navigation site-header__nav">
				<?php if ( get_theme_mod( 'company_nav_logo' ) ) : ?>
					<?php
					$bubbles_company_nav_logo = get_theme_mod( 'company_nav_logo' );
					?>
					<a href="https://www.pulsair.com" id="nav_logo" class="site-header__nav-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php echo esc_url($bubbles_company_nav_logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'description' ) ); ?>" data-rjs="2" data-no-lazy="1">
					</a>
				<?php else : ?>
					<h1 class="site-header__nav-title"><a href="https://www.pulsair.com>" title="<?php echo esc_attr( get_bloginfo( 'name', 'description' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php endif; ?>

				<?php wp_nav_menu($args);// extract the content from apperance-> nav menu ?>
			</nav> <!-- end #site-navigation -->
			<?php } ?>

	</div><!-- end #sticky_header  -->
</header> <!-- end #masthead -->
<!-- Main Page Start ============================================= -->
<main id="main-content" class="page--content container clearfix">
