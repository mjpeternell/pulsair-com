jQuery( function() {
	// Search toggle.
	jQuery(function(){
		var jQuerysearchlink = jQuery('#search-toggle');
		var jQuerysearchbox  = jQuery('#search-box');

		jQuery('#search-toggle').on('click', function(){

			if(jQuery(this).attr('id') == 'search-toggle') {
				if(!jQuerysearchbox.is(":visible")) {
					// if invisible we switch the icon to appear collapsable
					jQuerysearchlink.removeClass('header-search').addClass('header-search-x');
				} else {
					// if visible we switch the icon to appear as a toggle
					jQuerysearchlink.removeClass('header-search-x').addClass('header-search');
				}

				jQuerysearchbox.slideToggle(200, function(){
					// callback after search bar animation
				});
			}
		});
	});

	// Menu toggle for below 768 screens.
	( function() {
		var togglenav = jQuery( '.site-header__sticky' ), button, menu;
		if ( ! togglenav ) {
			return;
		}

		button = togglenav.find( '.menu-toggle' );
		if ( ! button ) {
			return;
		}

		menu = togglenav.find( '.menu' );
		if ( ! menu || ! menu.children().length ) {
			button.hide();
			return;
		}

		jQuery( '.menu-toggle' ).on( 'click', function() {
			jQuery(this).toggleClass("on");
			jQuery('body').toggleClass("no-scroll");
			togglenav.toggleClass( 'toggled-on' );
		} );
	} )();

} );
