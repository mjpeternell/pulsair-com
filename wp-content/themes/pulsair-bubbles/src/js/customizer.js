/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

(function($) {
  console.log(wp.customize);
  // Site title and description.

  wp.customize('blogname', function(value) {
    value.bind(function(to) {
      $('.site-title a').text(to);
    });
  });
  wp.customize('description', function(value) {
    value.bind(function(to) {
      $('.site-description').text(to);
    });
  });

  wp.customize('company_logo', function(value) {
    value.bind(function(to) {
      $('#site-logo img').attr("src", to);
    });
  });

  wp.customize('company_nav_logo', function(value) {
    value.bind(function(to) {
      $('#nav_logo img').attr("src", to);
    });
  });
})(jQuery);
