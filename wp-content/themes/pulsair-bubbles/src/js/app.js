import $ from 'jquery';

import whatInput from 'what-input';

import 'retinajs/dist/retina.min';
// import 'slick-carousel/slick/slick.min';

import './freesiaempire-main.js'
import './freesiaempire-sticky-scroll.js';
import './navigation.js';

$( document ).ready(function() {

    // Hide Go to top icon.
    $(".go-to-top").hide();

    $(window).scroll(function(){

        var windowScroll = $(window).scrollTop();
        if(windowScroll > 900)
        {
            $('.go-to-top').fadeIn();
        }
        else
        {
            $('.go-to-top').fadeOut();
        }
    });

    // Add height and width to embed video poster image for gt metrix
    var vid_poster = $('.mejs-poster');
    $('.mejs-poster-img').attr({ width: vid_poster.width(), height: vid_poster.height() });

    // scroll to Top on click
    $('.go-to-top').click(function(){
        $('html,header,body').animate({
            scrollTop: 0
        }, 700);
        return false;
    });
});

retinajs();
