<?php
/**
 * Template Name: WPB Full Width Page (Interior)
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

get_header();
?>
<?php
if ( has_post_thumbnail() ) {
?>
	<figure class="post-featured-image">
		<a href="<?php the_permalink(); ?>" title="<?php echo the_title_attribute('echo=0'); ?>">
			<?php the_post_thumbnail(); ?>
		</a>
	</figure><!-- end.post-featured-image  -->

<?php
}
?>
<div class="breadcrumb home">
	<?php the_breadcrumb(); ?>
</div> <!-- .breadcrumb -->
<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
        ?>
		<div class="entry-content">
			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<?php
            the_content();
			wp_link_pages(
                 array(
					 'before'            => '<div style="clear: both;"></div><div class="pagination clearfix">' . __( 'Pages:', 'pulsair-bubbles' ),
					 'after'             => '</div>',
					 'link_before'       => '<span>',
					 'link_after'        => '</span>',
					 'pagelink'          => '%',
					 'echo'              => 1,
				 )
                );
            ?>
		</div> <!-- entry-content clearfix-->
		<?php comments_template(); ?>
	<?php
    }
} else {
?>
	<h1 class="entry-title"> <?php _e( 'No Posts Found.', 'pulsair-bubbles' ); ?> </h1>
	<?php
}
?>
<?php

get_footer();
