<?php
/**
 * Template Name: Landing Page 2021
 *
 * Displays Corporate template.
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0.5
 */
get_header('landing-2021');
?>
	<?php
	// Start the loop.
	while ( have_posts() ) :
the_post();

		// Include the page content template.
		get_template_part( 'template-parts/content', 'landing-page' );

		// End of the loop.
	endwhile;
	?>
<?php
get_footer('landing');
