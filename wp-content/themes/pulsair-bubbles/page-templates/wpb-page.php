<?php
/**
 * Template Name: WPB Full Width Page
 *
 * Displays Corporate template.
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0.5
 */
get_header();
?>
	<?php
	// Start the loop.
	while ( have_posts() ) :
the_post();

		// Include the page content template.
		get_template_part( 'template-parts/content', 'wpb-page' );

		// End of the loop.
	endwhile;
	?>
<?php
get_footer();
