<?php
/**
 * Theme Common functions
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

/****************** freesiaempire DISPLAY COMMENT NAVIGATION *******************************/
function pulsair_comment_nav() {
	if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
	?>
	<nav class="navigation comment-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Comment navigation', 'pulsair-bubbles' ); ?></h2>
		<div class="nav-links">
			<?php
				$prev_link = get_previous_comments_link( _e( 'Older Comments', 'pulsair-bubbles' ) );
				if ( $prev_link ) :
				printf( '<div class="nav-previous">%s</div>', $prev_link );
				endif;

				$next_link = get_next_comments_link( _e( 'Newer Comments', 'pulsair-bubbles' ) );
				if ( $next_link ) :
				printf( '<div class="nav-next">%s</div>', $next_link );
				endif;
			?>
		</div><!-- .nav-links -->
	</nav><!-- .comment-navigation -->
	<?php
	endif;
}
/******************** Remove div and replace with ul*/
add_filter('wp_page_menu', 'pulsair_wp_page_menu');
function pulsair_wp_page_menu( $page_markup ) {
	preg_match('/^<div class=\"([a-z0-9-_]+)\">/i', $page_markup, $matches);
	$divclass   = $matches[1];
	$replace    = array('<div class="' . $divclass . '">', '</div>');
	$new_markup = str_replace($replace, '', $page_markup);
	$new_markup = preg_replace('/^<ul>/i', '<ul class="' . $divclass . '">', $new_markup);
	return $new_markup;
}


/**************************** Display Header Title ***********************************/
function pulsair_header_title() {
	$format = get_post_format();
	if ( is_archive() ) {
		if ( is_category() ) :
			$pulsair_header_title = single_cat_title( '', false );
		elseif ( is_tag() ) :
			$pulsair_header_title = single_tag_title( '', false );
		elseif ( is_author() ) :
			the_post();
			$pulsair_header_title = sprintf( _e( 'Author: %s', 'pulsair-bubbles' ), '<span class="vcard">' . get_the_author() . '</span>' );
			rewind_posts();
		elseif ( is_day() ) :
			$pulsair_header_title = sprintf( _e( 'Day: %s', 'pulsair-bubbles' ), '<span>' . get_the_date() . '</span>' );
		elseif ( is_month() ) :
			$pulsair_header_title = sprintf( _e( 'Month: %s', 'pulsair-bubbles' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );
		elseif ( is_year() ) :
			$pulsair_header_title = sprintf( _e( 'Year: %s', 'pulsair-bubbles' ), '<span>' . get_the_date( 'Y' ) . '</span>' );
		elseif ( $format === 'audio' ) :
			$pulsair_header_title = _e( 'Audios', 'pulsair-bubbles' );
		elseif ( $format === 'aside' ) :
			$pulsair_header_title = _e( 'Asides', 'pulsair-bubbles');
		elseif ( $format === 'image' ) :
			$pulsair_header_title = _e( 'Images', 'pulsair-bubbles' );
		elseif ( $format === 'gallery' ) :
			$pulsair_header_title = _e( 'Galleries', 'pulsair-bubbles' );
		elseif ( $format === 'video' ) :
			$pulsair_header_title = _e( 'Videos', 'pulsair-bubbles' );
		elseif ( $format === 'status' ) :
			$pulsair_header_title = _e( 'Status', 'pulsair-bubbles' );
		elseif ( $format === 'quote' ) :
			$pulsair_header_title = _e( 'Quotes', 'pulsair-bubbles' );
		elseif ( $format === 'link' ) :
			$pulsair_header_title = _e( 'links', 'pulsair-bubbles' );
		elseif ( $format === 'chat' ) :
			$pulsair_header_title = _e( 'Chats', 'pulsair-bubbles' );
		elseif ( class_exists('WooCommerce') && ( is_shop() || is_product_category() ) ) :
  			$pulsair_header_title = woocommerce_page_title( false );
  		elseif ( class_exists('bbPress') && is_bbpress() ) :
  			$pulsair_header_title = get_the_title();
		else :
			$pulsair_header_title = _e( 'Archives', 'pulsair-bubbles' );
		endif;
	} elseif ( is_home() ) {
		$pulsair_header_title = get_the_title( get_option( 'page_for_posts' ) );
	} elseif ( is_404() ) {
		$pulsair_header_title = _e('Page NOT Found', 'pulsair-bubbles');
	} elseif ( is_search() ) {
		$pulsair_header_title = _e('Search Results', 'pulsair-bubbles');
	} elseif ( is_page_template() ) {
		$pulsair_header_title = get_the_title();
	} else {
		$pulsair_header_title = get_the_title();
	}
	return $pulsair_header_title;
}
