<?php

function company_logo_register( $wp_customize ) {

    $wp_customize->add_setting( 'company_logo' ); // Add setting for logo uploader

    // Add control for logo uploader (actual uploader)
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
         $wp_customize,
        'company_logo',
        array(
			'label'    => __( 'Company Logo (replaces text)', 'pulsair-bubbles' ),
			'section'  => 'title_tagline',
			'settings' => 'company_logo',
		)
        )
    );

    $wp_customize->get_setting( 'company_logo' )->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial(
         'company_logo',
        array(
			'selector' => '#site-logo',
		)
        );

}
add_action( 'customize_register', 'company_logo_register' );


function company_nav_logo_register( $wp_customize ) {
    $wp_customize->add_setting( 'company_nav_logo' ); // Add setting for logo uploader

    // Add control for logo uploader (actual uploader)
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
         $wp_customize,
        'company_nav_logo',
        array(
			'label'       => __( 'Main Nav Logo', 'pulsair-bubbles' ),
			'description' => __( 'Logo appears when menu snaps to top of page. Ideal logo size for main nav is 50 x 37px and file type is SVG.' ),
            'section'     => 'title_tagline',
			'settings'    => 'company_nav_logo',
		)
        )
    );

    $wp_customize->get_setting( 'company_nav_logo' )->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial(
         'company_nav_logo',
        array(
			'selector' => '#nav_logo',
		)
        );

}
add_action( 'customize_register', 'company_nav_logo_register' );
