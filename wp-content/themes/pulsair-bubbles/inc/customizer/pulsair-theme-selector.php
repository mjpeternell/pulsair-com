<?php
function company_theme_register( $wp_customize ) {
    $wp_customize->add_section(
         'theme_options_section',
        array(
			'title'      => __( 'Theme Options', 'pulsair-bubbles' ),
			'priority'   => 30,
		)
        );

    $wp_customize->add_setting(
         'theme_option_setting',
        array(
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'theme_option_sanitize_select',
			'default'           => 'theme-style__default',
		)
        );

    $wp_customize->add_control(
         'theme_option_setting',
        array(
			'type'        => 'select',
			'section'     => 'theme_options_section', // Add a default or your own section
			'label'       => __( 'Select a Theme Option for Site.', 'pulsair-bubbles'),
			'description' => __( 'This will change the theme color palet.  Default is Pulsair Blue Theme.', 'pulsair-bubbles' ),
			'choices'     => array(
				'theme-style__default'      => __( 'Pulsair Blue Theme (Default Blue)', 'pulsair-bubbles' ),
				'theme-style__food-bev'     => __( 'Food & Beverage', 'pulsair-bubbles' ),
				'theme-style__wine'         => __( 'Wine (Red)', 'pulsair-bubbles' ),
				'theme-style__rail'         => __( 'Rail', 'pulsair-bubbles'),
				'theme-style__agricultural' => __( 'Agricultural (Green)', 'pulsair-bubbles' ),
			),
		)
        );

    $wp_customize->selective_refresh->add_partial(
         'theme_option_setting',
        array(
			'selector' => '#main-content',
		)
        );

    function theme_option_sanitize_select( $input, $setting ) {

        // Ensure input is a slug.
        $input = sanitize_key( $input );

        // Get list of choices from the control associated with the setting.
        $choices = $setting->manager->get_control( $setting->id )->choices;

        // If the input is a valid key, return it; otherwise, return the default.
        return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
    }
}
add_action( 'customize_register', 'company_theme_register' );
