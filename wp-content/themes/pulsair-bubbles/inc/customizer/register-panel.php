<?php
/**
 * Theme Customizer Functions
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */
 function mytheme_customize_register( $wp_customize ) {
   // All our sections, settings, and controls will be added here
   $wp_customize->remove_section( 'colors');
   $wp_customize->remove_section( 'background_image');
   $wp_customize->remove_section( 'custom_css');

 }
 add_action( 'customize_register', 'mytheme_customize_register', 50 );
