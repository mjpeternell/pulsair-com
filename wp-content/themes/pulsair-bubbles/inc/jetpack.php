<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */
/*********** FREESIAEMPIRE ADD THEME SUPPORT FOR INFINITE SCROLL **************************/
function pulsair_jetpack_setup() {
	add_theme_support(
         'infinite-scroll',
        array(
			'container' => 'main',
			'footer'    => 'page',
		)
        );
}
add_action( 'after_setup_theme', 'pulsair_jetpack_setup' );