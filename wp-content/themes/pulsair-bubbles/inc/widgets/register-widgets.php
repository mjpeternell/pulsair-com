<?php
/**
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */
/**************** FREESIAEMPIRE REGISTER WIDGETS ***************************************/
add_action('widgets_init', 'pulsair_widgets_init');
function pulsair_widgets_init() {
	register_widget('pulsair_contact_widgets' );

	register_sidebar(
        array(
			'name'          => __('Main Sidebar', 'pulsair-bubbles'),
			'id'            => 'pulsair_main_sidebar',
			'description'   => __('Shows widgets at Main Sidebar.', 'pulsair-bubbles'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
        );

	register_sidebar(
        array(
			'name'          => __('Footer Column 1', 'pulsair-bubbles'),
			'id'            => 'pulsair_footer_1',
			'description'   => __('Shows widgets at Footer Column 1', 'pulsair-bubbles'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
        );

	register_sidebar(
        array(
			'name'          => __('Footer Column 2', 'pulsair-bubbles'),
			'id'            => 'pulsair_footer_2',
			'description'   => __('Shows widgets at Footer Column 2', 'pulsair-bubbles'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
        );

	register_sidebar(
        array(
			'name'          => __('Footer Column 3', 'pulsair-bubbles'),
			'id'            => 'pulsair_footer_3',
			'description'   => __('Shows widgets at Footer Column 3', 'pulsair-bubbles'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
        );

	register_sidebar(
        array(
			'name'          => __('Footer Column 4', 'pulsair-bubbles'),
			'id'            => 'pulsair_footer_4',
			'description'   => __('Shows widgets at Footer Column 4', 'pulsair-bubbles'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
        );
}
