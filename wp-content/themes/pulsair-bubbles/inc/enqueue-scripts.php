<?php
/**
 * ENQUEING STYLES AND SCRIPTS
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

/*************************** ENQUEING STYLES AND SCRIPTS ****************************************/
function bubbles_scripts() {
	$pulsair_settings = bubbles_get_theme_options();
	wp_dequeue_style( 'js_composer_front' );
	wp_deregister_script('jquery');
	wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, '1.11.0', true);
   	wp_enqueue_script('jquery');
	$path = get_stylesheet_directory() . '/dist/css/app.css';
	$cache_bust = '?' . filemtime( $path );
	wp_enqueue_style( 'app-css', get_template_directory_uri() . '/dist/css/app.css', array('js_composer_front'), $cache_bust, 'screen' );
	wp_enqueue_script('app-js', get_template_directory_uri() . '/dist/js/app.js#asyncload', array('jquery'), $cache_bust, true);
	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/fonts/font-awesome.min.css', array(), $cache_bust);
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/dist/genericons/genericons.css', array(), $cache_bust);
	wp_enqueue_style( 'js_composer_front');
	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_template_directory_uri() . '/dist/js/html5.js', array(), '3.4.1', false );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bubbles_scripts', 99);

add_action( 'admin_enqueue_scripts', 'load_admin_styles' );
function load_admin_styles() {
	$cache_bust = '?' . filemtime( get_stylesheet_directory() . '/dist/css/app.css');
	wp_enqueue_style( 'admin_css_foo', get_template_directory_uri() . '/dist/css/editor.css', array(), $cache_bust );
}
