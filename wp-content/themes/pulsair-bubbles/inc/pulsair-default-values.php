<?php
if ( ! function_exists('bubbles_get_option_defaults_values') ) :
	/******************** FREESIAEMPIRE DEFAULT OPTION VALUES ******************************************/
	function bubbles_get_option_defaults_values() {
		global $bubbles_default_values;
		$bubbles_default_values = array(
			'pulsair_responsive'                   => 'on',
			'pulsair_animate_css'                  => 'on',
			'pulsair_design_layout'                => 'wide-layout',
			'pulsair_sidebar_layout_options'       => 'right',
			'pulsair_blog_layout_temp'             => 'large_image_display',
			'pulsair_enable_slider'                => 'frontpage',
			'pulsair_transition_effect'            => 'fade',
			'pulsair_transition_delay'             => '4',
			'pulsair_transition_duration'          => '1',
			'pulsair_search_custom_header'         => 0,
			'freesiaempire-img-upload-header-logo' => '',
			'pulsair_header_display'               => 'header_text',
			'pulsair_categories'                   => array(),
			'pulsair_custom_css'                   => '',
			'pulsair_scroll'                       => 0,
			'pulsair_custom_header_options'        => 'homepage',
			'pulsair_slider_link'                  => 0,
			'pulsair_tag_text'                     => esc_html__('Read More', 'pulsair-bubbles'),
			'pulsair_excerpt_length'               => '65',
			'pulsair_single_post_image'            => 'off',
			'pulsair_reset_all'                    => 0,
			'pulsair_stick_menu'                   => 0,
			'pulsair_blog_post_image'              => 'on',
			'pulsair_entry_format_blog'            => 'excerptblog_display',
			'pulsair_search_text'                  => esc_html__('Search &hellip;', 'pulsair-bubbles'),
			'pulsair_slider_type'                  => 'default_slider',
			'pulsair_slider_textposition'          => 'right',
			'pulsair_slider_no'                    => '4',
			'pulsair_slider_button'                => 0,
			'pulsair_total_features'               => '3',
			'pulsair_features_title'               => '',
			'pulsair_features_description'         => '',
			'pulsair_disable_features'             => 0,
			'pulsair_display_header_image'         => 'top',
			'pulsair_disable_features_alterpage'   => 0,
			'pulsair_footer_column_section'        => '4',
			'pulsair_entry_format_blog'            => 'show',
			'pulsair_entry_meta_blog'              => 'show-meta',
			'pulsair_slider_content'               => 'on',
			'pulsair_top_social_icons'             => 0,
			'pulsair_buttom_social_icons'          => 0,
			'pulsair_menu_position'                => 'middle',
			'pulsair_logo_display'                 => 'left',
			'pulsair_blog_content_layout'          => '',
			'pulsair_blog_header_display'          => 'show',
			'pulsair_display_page_featured_image'  => 0,
			'pulsair_crop_excerpt_length'          => 1,

		);
		return apply_filters( 'bubbles_get_option_defaults_values', $bubbles_default_values );
	}
endif;
