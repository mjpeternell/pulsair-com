<?php
/**
 * The template for displaying search results.
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */
get_header();
	$pulsair_settings = bubbles_get_theme_options();
	global $pulsair_content_layout;
	if ( $post ) {
	$layout = get_post_meta( $post->ID, 'pulsair_sidebarlayout', true );
	}
	if ( empty( $layout ) || is_archive() || is_search() || is_home() ) {
	$layout = 'default';
	}
	if ( 'default' == $layout ) { // Settings from customizer
	if ( ( $pulsair_settings['pulsair_sidebar_layout_options'] != 'nosidebar' ) && ( $pulsair_settings['pulsair_sidebar_layout_options'] != 'fullwidth' ) ) { ?>

<div id="primary">
	<?php
    }
	}
    ?>
	<div id="main">
	<?php
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			get_template_part( 'content', get_post_format() );
		}
	}
	else {
    ?>
	<h2 class="entry-title">
		<?php get_search_form(); ?>
		<p>&nbsp; </p>
		<?php _e( 'No Posts Found.', 'pulsair-bubbles' ); ?>
	</h2>
	<?php
	}
    ?>
	</div> <!-- #content -->
	<?php
    get_template_part( 'navigation', 'none' );
if ( 'default' == $layout ) { // Settings from customizer
		if ( ( $pulsair_settings['pulsair_sidebar_layout_options'] != 'nosidebar' ) && ( $pulsair_settings['pulsair_sidebar_layout_options'] != 'fullwidth' ) ) :
    ?>
</div> <!-- #primary -->
	<?php
			endif;
}
get_sidebar();
get_footer();