<?php
/**
 * Display all freesiaempire functions and definitions
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

if ( ! function_exists( 'bubbles_setup' ) ) :
	/**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
	function bubbles_setup() {
		/**
		* Set the content width based on the theme's design and stylesheet.
		*/
		global $content_width;
		if ( ! isset( $content_width ) ) {
			$content_width = 790;
		}

		/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on freesiaempire, use a find and replace
		* to change 'pulsair-bubbles' to the name of your theme in all the template files
		*/
		load_theme_textdomain( 'pulsair-bubbles', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		// add_theme_support( 'automatic-feed-links' );

		/*
		* Let WordPress manage the document title.
		*/
		add_theme_support( 'title-tag' );

		/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		*/
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in three location.
		register_nav_menus(
             array(
				 'primary' => __( 'Main Menu', 'pulsair-bubbles' ),
			 )
            );

		add_image_size('rectangle-xsmall', 150, 100);
		add_image_size('logos', 190, 190);
		add_image_size('rectangle-small', 300, 200);
		add_image_size('rectangle-medium', 600, 400, true);
		add_image_size('square-xsmall', 280, 280, true);
		add_image_size('square-small', 480, 480, true);
		add_image_size('square-medium', 680, 680, true);
		add_image_size('rectangle-large', 1024, 683, true);
		add_image_size('blog-hero', 1024, 380, true);
		add_image_size('pulsair_slider_image', 1920, 1080, true);

		// Register the three useful image sizes for use in Add Media modal
		add_filter( 'image_size_names_choose', 'bubbles_custom_sizes' );
		function bubbles_custom_sizes( $sizes ) {
			return array_merge(
                 $sizes,
                array(
					'square-xsmall' => __( 'Square Xs 280x280', 'pulsair-bubbles' ),
					'square-small'  => __( 'Square Sm 480x480', 'pulsair-bubbles' ),
					'square-medium' => __( 'Square Med 680x680', 'pulsair-bubbles' ),
				)
                );
		}

		/*
		* Switch default core markup for comment form, and comments
		* to output valid HTML5.
		*/
		add_theme_support(
             'html5',
            array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
            );

		/**
		* Add support for the Aside Post Formats
		*/
		add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio' ) );

		// Set up the WordPress core custom background feature.
		add_theme_support(
             'custom-background',
            apply_filters(
             'bubbles_custom_background_args',
            array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
            )
            );

		add_editor_style( array( '/css/editor-style.css', '/genericons/genericons.css' ) );

	}
endif; // bubbles_setup
add_action( 'after_setup_theme', 'bubbles_setup' );

if ( ! function_exists('bubbles_get_theme_options') ) :
	function bubbles_get_theme_options() {
		return wp_parse_args(  get_option( 'pulsair_theme_options', array() ), bubbles_get_option_defaults_values() );
	}
endif;

if ( function_exists('acf_add_options_page') ) {

	acf_add_options_page(
        array(
			'page_title'    => 'Theme General Settings',
			'menu_title'    => 'Theme Settings',
			'menu_slug'     => 'theme-general-settings',
			'capability'    => 'edit_posts',
			'redirect'      => false,
		)
        );

	acf_add_options_sub_page(
        array(
			'page_title'    => 'Theme Header Settings',
			'menu_title'    => 'Header',
			'parent_slug'   => 'theme-general-settings',
		)
        );

	acf_add_options_sub_page(
        array(
			'page_title'    => 'Theme Style',
			'menu_title'    => 'Theme Style',
			'parent_slug'   => 'theme-general-settings',
		)
        );

	acf_add_options_sub_page(
        array(
			'page_title'    => 'Theme Footer Settings',
			'menu_title'    => 'Footer',
			'parent_slug'   => 'theme-general-settings',
		)
        );

}

require get_template_directory() . '/vc-shortcodes/vc-overrides.php';
require get_template_directory() . '/inc/pulsair-default-values.php';
require get_template_directory() . '/inc/enqueue-scripts.php';
require get_template_directory() . '/inc/pulsair-wp-cleanup.php';
require get_template_directory() . '/inc/breadcrumb.php';
require get_template_directory() . '/inc/common-functions.php';
require get_template_directory() . '/inc/jetpack.php';
require get_template_directory() . '/inc/widgets/contactus-widgets.php';
require get_template_directory() . '/inc/widgets/register-widgets.php';
require get_template_directory() . '/inc/customizer/sanitize-functions.php';
require get_template_directory() . '/inc/customizer/register-panel.php';
require get_template_directory() . '/inc/customizer/pulsair-company-logo.php';
require get_template_directory() . '/inc/customizer/pulsair-theme-selector.php';

function bubbles_post_class_clearfix( $classes ) {
	$classes[] = 'clearfix';
	return $classes;
}
add_filter( 'post_class', 'bubbles_post_class_clearfix' );

function bubbles_remove_styles_sections( $wp_customize ) {
	$wp_customize->remove_control('site_icon');
}
add_action( 'customize_register', 'bubbles_remove_styles_sections', 20, 1 );

add_action('wp_head', 'bubbles_vc_override', 1);
function bubbles_vc_override() {
	if ( class_exists( 'Vc_Manager' ) ) {
		remove_action('wp_head', array(visual_composer(), 'addMetaData'));
	}
}

/* Async load function to add async to all scripts */
function bubbles_async_scripts( $url ) {
	if ( strpos( $url, '#asyncload') === false ) {
	return $url;
	} elseif ( is_admin() ) {
	return str_replace( '#asyncload', '', $url );
	} else {
return str_replace( '#asyncload', '', $url ) . "' async='async";
    }
}
add_filter( 'clean_url', 'bubbles_async_scripts', 11, 1 );
