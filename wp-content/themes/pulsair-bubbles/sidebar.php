<?php
/**
 * The sidebar containing the main Sidebar area.
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

 ?>
 <aside id="secondary" class="sidebar">
	  <?php dynamic_sidebar( 'pulsair_main_sidebar' ); ?>
</aside> <!-- #secondary -->
