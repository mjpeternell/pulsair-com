<?php
/**
 * The template for displaying the footer.
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

?>
</main><!-- end main -->
<!-- Footer Start ============================================= -->
<footer id="colophon" class="site-footer clearfix">
    <?php
    if ( is_active_sidebar( 'pulsair_footer_1' ) || is_active_sidebar( 'pulsair_footer_2' ) || is_active_sidebar( 'pulsair_footer_3' ) || is_active_sidebar( 'pulsair_footer_4' ) ) {
    ?>
        <div class="container">
            <div class="widget-area site-footer__widget-area clearfix">
                <div class="column-4">
                    <?php
                    if ( is_active_sidebar( 'pulsair_footer_1' ) ) :
                        dynamic_sidebar( 'pulsair_footer_1' );
                    endif;
                    ?>
                </div>
                <div class="column-4">
                    <?php
                    if ( is_active_sidebar( 'pulsair_footer_2' ) ) :
                        dynamic_sidebar( 'pulsair_footer_2' );
                    endif;
                    ?>
                </div>
                <div class="column-4">
                    <?php
                    if ( is_active_sidebar( 'pulsair_footer_3' ) ) :
                        dynamic_sidebar( 'pulsair_footer_3' );
                    endif;
                    ?>
                </div>
                <div class="column-4">
                    <?php
                    if ( is_active_sidebar( 'pulsair_footer_4' ) ) :
                        dynamic_sidebar( 'pulsair_footer_4' );
                    endif;
                    ?>
                </div>
            </div> <!-- end .widget-area -->
        </div> <!-- end .container -->
    <?php } ?>
    <div class="site-info site-footer__site-info">
        <div class="container">
            <div class="copyright">&copy; <?php echo date('Y'); ?>
                <a title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" target="_blank" href="https://www.pulsair.com"><?php echo get_bloginfo( 'name', 'display' ); ?></a>
            </div>
            <div style="clear:both;"></div>
        </div> <!-- end .container -->
    </div> <!-- end .site-info -->
    <div class="go-to-top"><a title="<?php esc_html_e('Go to Top', 'pulsair-bubbles'); ?>" href="#masthead"></a></div> <!-- end .go-to-top -->
</footer> <!-- end #colophon -->

<?php wp_footer(); ?>

<?php

$bubbles_urlparts = wp_parse_url( home_url() );
$bubbles_domain   = $bubbles_urlparts['host'];

if ( ( 'www.pulsair.com' === $bubbles_domain ) || ( 'pulsair.com' === $bubbles_domain ) ) {
?>
    <!-- Global site tag (gtag.js) - Google Ads: 997263229 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-997263229"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-997263229');
    </script>
<?php } else { ?>
    <!-- Global site tag (gtag.js) -<?php echo $bubbles_domain; ?> NOT TRACKING -->
<?php } ?>

</body>
</html>
