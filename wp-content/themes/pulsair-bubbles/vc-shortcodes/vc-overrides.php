<?php
/*
 * Filter to replace default css class names for vc_row shortcode and vc_column
 */

add_filter( 'vc_shortcodes_css_class', 'bubbles_css_classes_for_vc_btn', 10, 2);
function bubbles_css_classes_for_vc_btn( $class_string, $tag ) {

	if ( 'vc_toggle' === $tag ) {
		$class_string = str_replace( 'vc_toggle_default', 'vc_bubbles_toggle', $class_string ); // This will replace "vc_row-fluid" with "my_row-fluid"
		$class_string = str_replace( 'vc_toggle_color_default', '', $class_string );
		$class_string = str_replace( 'vc_toggle_size_md', '', $class_string );
	}

	if ( 'vc_btn' === $tag ) {
		$class_string = str_replace( 'vc_btn3-size-', '', $class_string ); // This will replace "vc_row-fluid" with "my_row-fluid"
		$class_string = str_replace( 'vc_btn3-shape-', '', $class_string );
		$class_string = str_replace( 'vc_btn3-style-', '', $class_string );
		$class_string = str_replace( 'vc_btn3-color-', '', $class_string );
		$class_string = str_replace( 'vc_general', 'theme-button', $class_string );

	}
	return $class_string; // Important: you should always return modified or original $class_string
}

add_action( 'vc_after_init', 'bubbles_wpb_button_init_actions' );
function bubbles_wpb_button_init_actions() {

	// Remove Params
	if ( function_exists( 'vc_remove_param' ) ) {
		vc_remove_param( 'vc_btn', 'shape' );
		vc_remove_param( 'vc_btn', 'add_icon' );
		vc_remove_param( 'vc_btn', 'i_align' );
		vc_remove_param( 'vc_btn', 'i_type' );
		vc_remove_param( 'vc_btn', 'i_icon_fontawesome' );
		vc_remove_param( 'vc_btn', 'i_icon_typicons' );
		vc_remove_param( 'vc_btn', 'i_icon_pixelicons' );
		vc_remove_param( 'vc_btn', 'i_icon_material' );
		vc_remove_param( 'vc_btn', 'i_icon_openiconic' );
		vc_remove_param( 'vc_btn', 'i_icon_monosocial' );
		vc_remove_param( 'vc_btn', 'i_icon_linecons' );
		vc_remove_param( 'vc_btn', 'i_icon_entypo' );
		vc_remove_param( 'vc_btn', 'color' );
		vc_remove_param( 'vc_btn', 'size' );
		vc_remove_param( 'vc_toggle', 'style' );
		vc_remove_param( 'vc_toggle', 'color' );
		vc_remove_param( 'vc_toggle', 'size' );
		vc_remove_param( 'vc_toggle', 'open' );

	}
}

add_action( 'vc_after_init', 'bubbles_wpb_change_button_style' );
function bubbles_wpb_change_button_style() {

	// Get current values stored in the color param in "Call to Action" element
	$param = WPBMap::getParam( 'vc_btn', 'style' );

	// Add New Colors to the 'value' array
	// btn-custom-1 and btn-custom-2 are the new classes that will be
	// applied to your buttons, and you can add your own style declarations
	// to your stylesheet to style them the way you want.
	// $param['value'][__( 'WYS Green Text', 'pulsair-bubbles' )] = 'wys-green-btn';
	$param['value'][ __( 'Pulsair Button - Blue (default)', 'pulsair-bubbles' ) ]           = 'vc_btn3--default-btn';
	$param['value'][ __( 'Pulsair Button - Solid Dark - Wine', 'pulsair-bubbles' ) ]        = 'vc_btn3--dark-wine-btn';
	$param['value'][ __( 'Pulsair Button - Solid Dark - Food & Bev', 'pulsair-bubbles' ) ]  = 'solid-dark--food ';
	$param['value'][ __( 'Pulsair Button - Solid Dark - Rail', 'pulsair-bubbles' ) ]        = 'solid-dark--rail ';
	$param['value'][ __( 'Pulsair Button - Solid Dark Green - Agriculture', 'pulsair-bubbles' ) ] = 'vc_btn3--agri-green-btn';

	// Remove any colors you don't want to use.
	unset( $param['value']['Modern'] );
	unset( $param['value']['Classic'] );
	unset( $param['value']['Flat'] );
	unset( $param['value']['Outline'] );
	unset( $param['value']['3d'] );
	unset( $param['value']['Custom'] );
	unset( $param['value']['Gradient'] );
	unset( $param['value']['Gradient Custom'] );
	unset( $param['value']['Outline custom'] );

	// Finally "update" with the new values
	vc_update_shortcode_param( 'vc_btn', $param );
}

add_action( 'vc_after_init', 'bubbles_wpb_change_button_alignment' );
function bubbles_wpb_change_button_alignment() {

	// Get current values stored in the color param in "Call to Action" element
	$param = WPBMap::getParam( 'vc_btn', 'align' );
	$param['value'][ __( 'Block', 'pulsair-bubbles' ) ] = 'block ';

	// Remove any colors you don't want to use.
	// unset( $param['value']['Modern'] );

	// Finally "update" with the new values
	vc_update_shortcode_param( 'vc_btn', $param );
}
