<?php
/**
 * Displays the searchform
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */
 
?>
<form class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
	<?php
		$pulsair_settings    = bubbles_get_theme_options();
		$pulsair_search_form = $pulsair_settings['pulsair_search_text'];
		if ( 'Search &hellip;' !== $pulsair_search_form ) :
        ?>
	<input type="search" name="s" class="search-field" placeholder="<?php echo esc_attr($pulsair_search_form); ?>" autocomplete="off">
	<button type="submit" class="search-submit"><i class="search-icon"></i></button>
	<?php else : ?>
	<input type="search" name="s" class="search-field" placeholder="<?php esc_attr_e( 'Search ...', 'pulsair-bubbles' ); ?>" autocomplete="off">
	<button type="submit" class="search-submit"><i class="search-icon"></i></button>
	<?php endif; ?>
</form> <!-- end .search-form -->
