<?php
/**
* Displays the header content
*
* @package Theme Pulsair
* @subpackage Pulsair Bubbles
* @since Pulsair Bubbles 1.0
*/
?>

<?php

if ( get_field( 'lp_theme_color' ) ) {
	$lp_theme_option = get_field( 'lp_theme_color' ) ;
}

?>
<!DOCTYPE html>
<html class="no-js <?php echo esc_attr($lp_theme_option); ?>" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	<?php get_template_part( 'template-parts/favicons' ); ?>
</head>
<body <?php body_class(); ?>>
	<!-- Masthead ============================================= -->
	<header id="masthead" class="site-header top-header lp-header">
			<div class="container clearfix lp-header__container">
				<div id="site-branding" class="lp-header__branding">
					<?php if ( get_theme_mod( 'company_logo' ) ) : ?>
						<a href="https://www.pulsair.com" id="site-logo" class="lp-header__logo site-header__logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							<img src="/wp-content/themes/pulsair-bubbles/dist/images/pulsair-logo-vector-white-small.svg" data-rjs="2" data-no-lazy="1">
						</a>
					<?php endif; ?>
				</div>
				<div id="site-branding" class="lp-header__title">
					<hgroup>
						<h1 class="site-title"><?php echo the_title(); ?></h1>
					</hgroup>
				</div>
				<div class="top-request aaa lp-header__contact">
					<ul>

						<?php
						$lp_phone = get_field('landing_page_phone_number');
						$lp_text  = get_field('landing_page_text');

						if ( $lp_phone ) {
							?>
							<li><a href="tel:<?php echo $lp_phone; ?>"><?php echo $lp_phone; ?></a></li>
						<?php } else { ?>
							<li><a href="tel:800.582.7797">800.582.7797</a></li>
						<?php } ?>
					</ul>
				</div>



			</div> <!-- end .container -->
		<!-- Main Header============================================= -->
	</header> <!-- end #masthead -->
	<!-- Main Page Start ============================================= -->
	<main id="main-content" class="page--content container clearfix">
