<?php
/**
 * The template for displaying all single posts.
 *
 * @package Theme Pulsair
 * @subpackage Pulsair Bubbles
 * @since Pulsair Bubbles 1.0
 */

get_header();
?>
<section id="primary">
	<?php
    global $pulsair_settings;
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
            ?>
			<?php $bubbles_format = get_post_format(); ?>
			<article <?php post_class('post-format' . ' format-' . $bubbles_format); ?> id="post-<?php the_ID(); ?>">
				<?php
				if ( has_post_thumbnail() ) {
                ?>
					<figure class="post-featured-image">
						<a href="<?php the_permalink(); ?>" title="<?php echo the_title_attribute('echo=0'); ?>">
						<?php the_post_thumbnail('blog-hero'); ?>
						</a>
					</figure><!-- end.post-featured-image  -->
				<?php } ?>
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<div class="entry-meta">
						<?php
							if ( current_theme_supports( 'post-formats', $bubbles_format ) ) {
							printf(
						 '<span class="entry-format">%1$s<a href="%2$s">%3$s</a></span>',
							sprintf( ''),
							esc_url( get_post_format_link( $bubbles_format ) ),
							get_post_format_string( $bubbles_format )
						);
							}
                            ?>
						<span class="author vcard"><?php esc_html_e('Author :', 'pulsair-bubbles'); ?><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php the_author(); ?>">
						<?php the_author(); ?> </a></span> <span class="posted-on"><?php esc_html_e('Date  :', 'pulsair-bubbles'); ?><a title="<?php echo esc_attr( get_the_time() ); ?>" href="<?php the_permalink(); ?>">
						<?php the_time( get_option( 'date_format' ) ); ?> </a></span>
						<?php if ( comments_open() ) { ?>
						<span class="comments"><?php esc_html_e('Comment  :', 'pulsair-bubbles'); ?>
							<?php comments_popup_link( esc_html_e( 'No Comments', 'pulsair-bubbles' ), esc_html_e( '1 Comment', 'pulsair-bubbles' ), esc_html_e( '% Comments', 'pulsair-bubbles' ), '', esc_html_e( 'Comments Off', 'pulsair-bubbles' ) ); ?>
						</span>
						<?php } ?>
					</div> <!-- .entry-meta -->

				</header> <!-- .entry-header -->
				<div class="entry-content">
				<?php
                the_content();
					wp_link_pages(
                         array(
							 'before'        => '<div style="clear: both;"></div><div class="pagination clearfix">' . esc_html_e( 'Pages:', 'pulsair-bubbles' ),
							 'after'         => '</div>',
							 'link_before'   => '<span>',
							 'link_after'    => '</span>',
							 'pagelink'      => '%',
							 'echo'          => 1,
						 )
                        );
                    ?>
				</div> <!-- .end entry-content -->
				<?php
                if ( is_single() ) {
					$bubbles_tag_list     = get_the_tag_list( '', esc_html_e( ' ', 'pulsair-bubbles' ) );
					$bubbles_entry_format = $pulsair_settings['pulsair_entry_format_blog'];
					if ( ( 'show' === $bubbles_entry_format ) || ( 'show-button' === $bubbles_entry_format ) || ( 'hide-button' === $bubbles_entry_format ) ) {
                    ?>
					<footer class="entry-footer">
						<span class="cat-links">
						<?php
						esc_html_e('Category : ', 'pulsair-bubbles');
						the_category(', ');
						?>
						</span> <!-- .cat-links -->
						<?php
                        $bubbles_tag_list = get_the_tag_list( '', esc_html_e( ', ', 'pulsair-bubbles' ) );
							if ( ! empty($bubbles_tag_list) ) {
                            ?>
							<span class="tag-links">  <?php echo $bubbles_tag_list; ?> </span> <!-- .tag-links -->
							<?php } ?>
					</footer> <!-- .entry-meta -->
					<?php
                    }
				}
                ?>
			</article>
	<?php
    }
		}
	else {
    ?>
	<h1 class="entry-title"> <?php esc_html_e( 'No Posts Found.', 'pulsair-bubbles' ); ?> </h1>
	<?php } ?>

	<?php
	 if ( is_single() ) {
		if ( is_attachment() ) {
        ?>
		<ul class="default-wp-page clearfix">
			<li class="previous"> <?php previous_image_link( false, esc_html_e( '&larr; Previous', 'pulsair-bubbles' ) ); ?> </li>
			<li class="next">  <?php next_image_link( false, esc_html_e( 'Next &rarr;', 'pulsair-bubbles' ) ); ?> </li>
		</ul>
		<?php } else { ?>
		<ul class="default-wp-page clearfix">
			<li class="previous"> <?php previous_post_link( '%link', '<span class="meta-nav">' . esc_html_x( '&larr;', 'Previous post link', 'pulsair-bubbles' ) . '</span> %title' ); ?> </li>
			<li class="next"> <?php next_post_link( '%link', '%title <span class="meta-nav">' . esc_html_x( '&rarr;', 'Next post link', 'pulsair-bubbles' ) . '</span>' ); ?> </li>
		</ul>
			<?php
            }
					}
	comments_template();
?>
</section> <!-- #primary -->
<?php

get_sidebar();
get_footer();
